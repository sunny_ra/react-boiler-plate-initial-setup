import React from 'react';
//router
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
const App = () => {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={() => {}} />
          <Route exact path="*" component={() => {}} />
        </Switch>
      </Router>
    </>
  );
};

export default App;
